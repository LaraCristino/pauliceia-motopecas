﻿Public Class frm_estoque

    Private Sub frm_estoque_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conexao_banco()
        dados_grid_estoque()
    End Sub

    Private Sub btn_gravar_Click(sender As Object, e As EventArgs) 
        sql = "select * from tb_produtos where descricao = '" & txt_descricao.Text & "'"
        rs = db.Execute(sql)
        If txt_descricao.Text = "" Or txt_preco.Text = "" Or txt_qtde.Text = "" Then
            MsgBox("Não foi possível adicionar produto, verifique os campos!")
        Else
            If rs.EOF = True Then
                gerar_id_estoque()
                sql = "insert into tb_produtos values (" & id & ", '" & txt_descricao.Text & "', '" & txt_preco.Text & "', " &
                " ' " & txt_qtde.Text & " ' )"
                rs = db.Execute(UCase(sql))
                MsgBox("Dados gravados com sucesso!")
                txt_descricao.Clear()
                txt_preco.Clear()
                txt_qtde.Clear()
                dados_grid_estoque()
            End If
        End If
    End Sub

    Private Sub txt_busca_TextChanged(sender As Object, e As EventArgs) 
        With dgw_estoque
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_produtos where " & "descricao" & " like '" & txt_descricao.Text & "%'"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Private Sub dgw_estoque_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) 

        With dgw_estoque
            aux = .CurrentRow.Cells(1).Value.ToString
            If .CurrentRow.Cells(4).Selected = True Then
                resp = MsgBox("Deseja realmente excluir?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                If resp = MsgBoxResult.Yes Then
                    sql = "delete * from tb_produtos where descricao ='" & aux & "'"
                    rs = db.Execute(sql)
                    dados_grid_estoque()
                End If
            End If
        End With
    End Sub

    Private Sub Txt_preco_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) 
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Txt_qtde_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub
    Private Sub btn_sair_Click_1(sender As Object, e As EventArgs) Handles btn_sair.Click
        frm_login.Show()
        Me.Close()
    End Sub

    Private Sub btn_home_Click_1(sender As Object, e As EventArgs) Handles btn_home.Click
        frm_home.Show()
        Me.Close()
    End Sub
End Class