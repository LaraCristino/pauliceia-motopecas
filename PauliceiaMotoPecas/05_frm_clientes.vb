﻿Public Class _05_frm_clientes

    Private Sub _05_frm_clientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        With cmb_opcoes.Items
            .Add("Clientes")
            .Add("Fornecedores")
        End With
        cmb_opcoes.SelectedIndex = 0
        conexao_banco()
        dados_grid_clientes()
        dados_grid_fornecedores()
        btn_atualizar.Enabled = False
        btn_cancelar.Enabled = False

        AddHandler txt_nome_p_c.TextChanged, AddressOf txt_nome_p_c_TextChanged
        AddHandler txt_cpf_p_c.TextChanged, AddressOf txt_cpf_p_c_TextChanged
        AddHandler txt_nome_p_f.TextChanged, AddressOf txt_nome_p_f_TextChanged
        AddHandler txt_cnpj_p_f.TextChanged, AddressOf txt_cnpj_p_f_TextChanged

    End Sub

    Private Sub txt_cpf_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_cpf.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txt_celular_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_celular.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txt_cpf_p_c_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_cpf_p_c.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txt_cnpj_p_f_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_cnpj_p_f.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btn_gravar_Click(sender As Object, e As EventArgs) Handles btn_gravar.Click
        op = cmb_opcoes.Text
        If op = "Clientes" Then
            sql = "select * from tb_clientes where nome = '" & txt_nome.Text & "'"
            rs = db.Execute(sql)
            If txt_nome.Text = "" Or txt_cpf.Text = "" Or txt_email.Text = "" Or txt_celular.Text = "" Then
                MsgBox("Não foi possível adicionar produto, verifique os campos!")
            Else
                If rs.EOF = True Then
                    gerar_id_clientes()
                    sql = "insert into tb_clientes values (" & id & ", '" & txt_nome.Text & "', '" & txt_cpf.Text & "', " &
                    " ' " & txt_email.Text & " ', '" & txt_celular.Text & "' )"
                    rs = db.Execute(UCase(sql))
                    MsgBox("Dados gravados com sucesso!")
                    txt_nome.Clear()
                    txt_cpf.Clear()
                    txt_email.Clear()
                    txt_celular.Clear()
                    dados_grid_clientes()
                End If
            End If
        ElseIf op = "Fornecedores" Then
            sql = "select * from tb_fornecedores where nome = '" & txt_nome.Text & "'"
            rs = db.Execute(sql)
            If txt_nome.Text = "" Or txt_cpf.Text = "" Or txt_email.Text = "" Or txt_celular.Text = "" Then
                MsgBox("Não foi possível adicionar produto, verifique os campos!")
            Else
                If rs.EOF = True Then
                    gerar_id_fornecedores()
                    sql = "insert into tb_fornecedores values (" & id & ", '" & txt_nome.Text & "', '" & txt_cpf.Text & "', " &
                    " ' " & txt_email.Text & " ', '" & txt_celular.Text & "' )"
                    rs = db.Execute(UCase(sql))
                    MsgBox("Dados gravados com sucesso!")
                    txt_nome.Clear()
                    txt_cpf.Clear()
                    txt_email.Clear()
                    txt_celular.Clear()
                    dados_grid_fornecedores()
                End If
            End If
        End If
    End Sub

    Private Sub btn_atualizar_Click(sender As Object, e As EventArgs) Handles btn_atualizar.Click
        op = cmb_opcoes.Text
        If op = "Clientes" Then
            sql = "select * from tb_clientes where nome = '" & aux & "'"
            rs = db.Execute(sql)
            If txt_nome.Text = "" Or txt_cpf.Text = "" Or txt_email.Text = "" Or txt_celular.Text = "" Then
                MsgBox("Não foi possível adicionar produto, verifique os campos!")
            Else
                If rs.BOF = False Then
                    sql = "update tb_clientes Set nome = '" & txt_nome.Text & "', cpf = '" & txt_cpf.Text & "', " &
                    " email = '" & txt_email.Text & "', celular = '" & txt_celular.Text & "' where nome = '" & aux & "'"
                    rs = db.Execute(UCase(sql))
                    MsgBox("Dados atualizados com sucesso!")
                    txt_nome.Clear()
                    txt_cpf.Clear()
                    txt_email.Clear()
                    txt_celular.Clear()
                    dados_grid_clientes()
                    btn_gravar.Enabled = True
                    btn_atualizar.Enabled = False
                    btn_cancelar.Enabled = False
                    cmb_opcoes.Enabled = True
                End If
            End If
        ElseIf op = "Fornecedores" Then
            sql = "select * from tb_fornecedores where nome ='" & aux & "'"
            rs = db.Execute(sql)
            If txt_nome.Text = "" Or txt_cpf.Text = "" Or txt_email.Text = "" Or txt_celular.Text = "" Then
                MsgBox("Não foi possível adicionar produto, verifique os campos!")
            Else
                If rs.BOF = False Then
                    sql = "update tb_fornecedores Set nome = '" & txt_nome.Text & "', cnpj = '" & txt_cpf.Text & "', " &
                    " email = '" & txt_email.Text & "', celular = '" & txt_celular.Text & "' where nome = '" & aux & "'"
                    rs = db.Execute(UCase(sql))
                    MsgBox("Dados atualizados com sucesso!")
                    txt_nome.Clear()
                    txt_cpf.Clear()
                    txt_email.Clear()
                    txt_celular.Clear()
                    dados_grid_fornecedores()
                    btn_gravar.Enabled = True
                    btn_atualizar.Enabled = False
                    btn_cancelar.Enabled = False
                    cmb_opcoes.Enabled = True
                Else
                    MsgBox("erro")
                End If
            End If
        End If
    End Sub

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        txt_nome.Clear()
        txt_cpf.Clear()
        txt_email.Clear()
        txt_celular.Clear()
        btn_gravar.Enabled = True
        btn_atualizar.Enabled = False
        btn_cancelar.Enabled = False
        cmb_opcoes.Enabled = True
    End Sub

    Private Sub cmb_opcoes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_opcoes.SelectedIndexChanged
        txt_nome.Clear()
        txt_cpf.Clear()
        txt_email.Clear()
        txt_celular.Clear()
        op = cmb_opcoes.Text
        If op = "Fornecedores" Then
            Label5.Text = "CNPJ"
            txt_cpf.Mask = "99,999,999/9999-99"
        Else
            Label5.Text = "CPF"
            txt_cpf.Mask = "999,999,999-99"
        End If
    End Sub

    Private Sub dgw_clientes_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgw_clientes.CellContentClick
        Try
            With dgw_clientes
                aux = .CurrentRow.Cells(1).Value.ToString
                If .CurrentRow.Cells(6).Selected = True Then
                    resp = MsgBox("Deseja realmente excluir?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                    If resp = MsgBoxResult.Yes Then
                        sql = "delete * from tb_clientes where nome ='" & aux & "'"
                        rs = db.Execute(sql)
                        dados_grid_clientes()
                    End If
                ElseIf .CurrentRow.Cells(5).selected = True Then
                    resp = MsgBox("Deseja editar as informações?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                    If resp = MsgBoxResult.Yes Then
                        sql = "select * from tb_clientes where nome = '" & aux & "'"
                        rs = db.Execute(sql)
                        If rs.EOF = False Then
                            TabControl1.SelectTab(0)
                            txt_nome.Text = rs.Fields(1).Value
                            txt_cpf.Text = rs.Fields(2).Value
                            txt_email.Text = rs.Fields(3).Value
                            txt_celular.Text = rs.Fields(4).Value
                            btn_atualizar.Enabled = True
                            btn_cancelar.Enabled = True
                            btn_gravar.Enabled = False
                            cmb_opcoes.Enabled = False
                        End If
                    End If
                Else
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            MsgBox("Erro ao processar!")
        End Try
    End Sub

    Private Sub dgw_fornecedores_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgw_fornecedores.CellContentClick
        Try
            With dgw_fornecedores
                aux = .CurrentRow.Cells(1).Value.ToString
                If .CurrentRow.Cells(6).Selected = True Then
                    resp = MsgBox("Deseja realmente excluir?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                    If resp = MsgBoxResult.Yes Then
                        sql = "delete * from tb_fornecedores where nome ='" & aux & "'"
                        rs = db.Execute(sql)
                        dados_grid_fornecedores()
                    End If
                ElseIf .CurrentRow.Cells(5).Selected = True Then
                    resp = MsgBox("Deseja editar as informações?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                    If resp = MsgBoxResult.Yes Then
                        sql = "select * from tb_fornecedores where nome ='" & aux & "'"
                        rs = db.Execute(sql)
                        If rs.EOF = False Then
                            TabControl1.SelectTab(0)
                            cmb_opcoes.SelectedIndex = 1
                            txt_nome.Text = rs.Fields(1).Value
                            txt_cpf.Text = rs.Fields(2).Value
                            txt_email.Text = rs.Fields(3).Value
                            txt_celular.Text = rs.Fields(4).Value
                            btn_atualizar.Enabled = True
                            btn_cancelar.Enabled = True
                            btn_gravar.Enabled = False
                            cmb_opcoes.Enabled = False
                        End If
                    End If
                Else
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            MsgBox("Erro ao processar!")
        End Try
    End Sub

    Private Sub txt_nome_p_c_TextChanged(sender As Object, e As EventArgs) Handles txt_nome_p_c.TextChanged

    End Sub

    Private Sub txt_cpf_p_c_TextChanged(sender As Object, e As EventArgs) Handles txt_cpf_p_c.TextChanged

    End Sub

    Private Sub txt_nome_p_c_gotfocus(sender As Object, e As EventArgs) Handles txt_nome_p_c.GotFocus
        If txt_nome_p_c.Text = "Nome" Then
            txt_nome_p_c.Text = ""
            dados_grid_clientes()
        End If
    End Sub

    Private Sub txt_nome_p_c_lostfocus(sender As Object, e As EventArgs) Handles txt_nome_p_c.LostFocus
        If txt_nome_p_c.Text = "" Then
            txt_nome_p_c.Text = "Nome"
            dados_grid_clientes()
        End If
    End Sub

    Private Sub txt_cpf_p_c_gotfocus(sender As Object, e As EventArgs) Handles txt_cpf_p_c.GotFocus
        If txt_cpf_p_c.Text = "CPF" Then
            txt_cpf_p_c.Text = ""
            dados_grid_clientes()
        End If
    End Sub

    Private Sub txt_cpf_p_c_lostfocus(sender As Object, e As EventArgs) Handles txt_cpf_p_c.LostFocus
        If txt_cpf_p_c.Text = "" Then
            txt_cpf_p_c.Text = "CPF"
            dados_grid_clientes()
        End If
    End Sub

    Private Sub txt_nome_p_f_TextChanged(sender As Object, e As EventArgs)
        With dgw_fornecedores
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_fornecedores where " & "nome" & " like '" & txt_nome_p_f.Text & "%'"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, rs.Fields(4).Value, Nothing, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Private Sub txt_cnpj_p_f_TextChanged(sender As Object, e As EventArgs)
        With dgw_fornecedores
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_fornecedores where " & "cnpj" & " like '" & txt_cnpj_p_f.Text & "%'"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, rs.Fields(4).Value, Nothing, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Private Sub txt_nome_p_f_gotfocus(sender As Object, e As EventArgs) Handles txt_nome_p_f.GotFocus
        If txt_nome_p_f.Text = "Nome" Then
            txt_nome_p_f.Text = ""
            dados_grid_fornecedores()
        End If
    End Sub

    Private Sub txt_nome_p_f_lostfocus(sender As Object, e As EventArgs) Handles txt_nome_p_f.LostFocus
        If txt_nome_p_f.Text = "" Then
            txt_nome_p_f.Text = "Nome"
            dados_grid_fornecedores()
        End If
    End Sub

    Private Sub txt_cnpj_p_f_gotfocus(sender As Object, e As EventArgs) Handles txt_cnpj_p_f.GotFocus
        If txt_cnpj_p_f.Text = "CNPJ" Then
            txt_cnpj_p_f.Text = ""
            dados_grid_fornecedores()
        End If
    End Sub

    Private Sub txt_cnpj_p_f_lostfocus(sender As Object, e As EventArgs) Handles txt_cnpj_p_f.LostFocus
        If txt_cnpj_p_f.Text = "" Then
            txt_cnpj_p_f.Text = "CNPJ"
            dados_grid_fornecedores()
        End If
    End Sub
    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        frm_home.Show()
        Me.Close()
    End Sub
    Private Sub btn_sair_Click(sender As Object, e As EventArgs) Handles btn_sair.Click
        frm_login.Show()
        Me.Close()
    End Sub
End Class