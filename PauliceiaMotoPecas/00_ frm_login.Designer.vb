﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_login))
        Me.btn_entrar = New System.Windows.Forms.Button()
        Me.mkb_senha = New System.Windows.Forms.MaskedTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_nome = New System.Windows.Forms.TextBox()
        Me.lbl_senhainvalida = New System.Windows.Forms.Label()
        Me.lbl_logado = New System.Windows.Forms.Label()
        Me.btn_sair = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_entrar
        '
        Me.btn_entrar.BackColor = System.Drawing.Color.Transparent
        Me.btn_entrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_entrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_entrar.ForeColor = System.Drawing.Color.DarkOrange
        Me.btn_entrar.Location = New System.Drawing.Point(641, 293)
        Me.btn_entrar.Name = "btn_entrar"
        Me.btn_entrar.Size = New System.Drawing.Size(84, 34)
        Me.btn_entrar.TabIndex = 9
        Me.btn_entrar.Text = "ENTRAR"
        Me.btn_entrar.UseVisualStyleBackColor = False
        '
        'mkb_senha
        '
        Me.mkb_senha.Location = New System.Drawing.Point(530, 218)
        Me.mkb_senha.Name = "mkb_senha"
        Me.mkb_senha.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.mkb_senha.Size = New System.Drawing.Size(237, 20)
        Me.mkb_senha.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Impact", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(477, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(326, 45)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Paulicéia Moto Peças"
        '
        'txt_nome
        '
        Me.txt_nome.Location = New System.Drawing.Point(530, 179)
        Me.txt_nome.Name = "txt_nome"
        Me.txt_nome.Size = New System.Drawing.Size(237, 20)
        Me.txt_nome.TabIndex = 7
        '
        'lbl_senhainvalida
        '
        Me.lbl_senhainvalida.AutoSize = True
        Me.lbl_senhainvalida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_senhainvalida.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_senhainvalida.Location = New System.Drawing.Point(519, 252)
        Me.lbl_senhainvalida.Name = "lbl_senhainvalida"
        Me.lbl_senhainvalida.Size = New System.Drawing.Size(262, 13)
        Me.lbl_senhainvalida.TabIndex = 12
        Me.lbl_senhainvalida.Text = "Senha ou Usuários Inválidos! Contate o administrador!"
        '
        'lbl_logado
        '
        Me.lbl_logado.AutoSize = True
        Me.lbl_logado.Location = New System.Drawing.Point(531, 155)
        Me.lbl_logado.Name = "lbl_logado"
        Me.lbl_logado.Size = New System.Drawing.Size(0, 13)
        Me.lbl_logado.TabIndex = 13
        '
        'btn_sair
        '
        Me.btn_sair.BackColor = System.Drawing.Color.Transparent
        Me.btn_sair.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_sair.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_sair.ForeColor = System.Drawing.Color.DarkOrange
        Me.btn_sair.Location = New System.Drawing.Point(551, 293)
        Me.btn_sair.Name = "btn_sair"
        Me.btn_sair.Size = New System.Drawing.Size(84, 34)
        Me.btn_sair.TabIndex = 14
        Me.btn_sair.Text = "SAIR"
        Me.btn_sair.UseVisualStyleBackColor = False
        '
        'frm_login
        '
        Me.AcceptButton = Me.btn_entrar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightGray
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(800, 473)
        Me.Controls.Add(Me.btn_sair)
        Me.Controls.Add(Me.lbl_logado)
        Me.Controls.Add(Me.lbl_senhainvalida)
        Me.Controls.Add(Me.btn_entrar)
        Me.Controls.Add(Me.mkb_senha)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_nome)
        Me.DoubleBuffered = True
        Me.Name = "frm_login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_entrar As Button
    Friend WithEvents mkb_senha As MaskedTextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_nome As TextBox
    Friend WithEvents lbl_senhainvalida As Label
    Friend WithEvents lbl_logado As Label
    Friend WithEvents btn_sair As Button
End Class
