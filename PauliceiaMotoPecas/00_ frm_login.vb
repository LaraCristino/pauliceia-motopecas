﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frm_login

    Public Function validacampos(ByVal login As String, ByVal senha As String)
        Dim data As OleDbDataReader = Nothing
        Const sql = "select * from tb_config WHERE usuario =? and senha=?"
        Using con As OleDbConnection = GetConnection()
            Using cmd As New OleDbCommand(sql, con)
                con.Open()
                cmd.Parameters.AddWithValue("@p1", login)
                cmd.Parameters.AddWithValue("@p2", senha)
                data = cmd.ExecuteReader()
                If data.HasRows Then
                    data.Read()
                    Modulo_geral.tipoUsuarioLogado = data("tipo")
                    Return True
                Else
                    lbl_senhainvalida.Visible = True
                    Return False
                End If
            End Using
        End Using

    End Function

    Private Sub frm_login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_senhainvalida.Visible = False
    End Sub

    Private Sub btn_entrar_Click_1(sender As Object, e As EventArgs) Handles btn_entrar.Click
        If validacampos(txt_nome.Text, mkb_senha.Text) Then
            frm_home.Show()
            txt_nome.Clear()
            mkb_senha.Clear()
            lbl_senhainvalida.Visible = False
        End If
    End Sub

    Private Sub btn_sair_Click(sender As Object, e As EventArgs) Handles btn_sair.Click
        If MsgBox("Deseja encerrar o aplicativo?", vbQuestion + vbYesNo, "Sair do Sistema") = vbYes Then
            Application.Exit()
        End If
    End Sub

End Class
