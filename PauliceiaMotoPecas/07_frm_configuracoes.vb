﻿Imports System.Data.OleDb
Imports System.Data
Public Class _07_frm_configuracoes
    Dim conn = New OleDbConnection
    Dim status_acesso As Boolean
    Dim itensComboAcesso As String = ""
    Dim dt As DataTable

    Private Sub frm_configuracoes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txt_pesquisar.Clear()
        btn_pesquisar_Click(Nothing, Nothing)

        lbl_id.Visible = False
        txt_nomeusuario.Clear()
        txt_senha.Clear()
        With Me.cmb_bloquear.Items
            .Add("Usuário Liberado")
            .Add("Usuário Bloqueado")
        End With
        With Me.cmbTipoUsuario.Items
            .Add("INTERMEDIARIO")
            .Add("LIMITADO")
            .Add("ADMINISTRADOR")
        End With
    End Sub
    Private Sub btn_adicionar_Click(sender As Object, e As EventArgs) Handles btnAdicionarNovo.Click
        Using conn = Modulo_geral.GetConnection()
            conn.Open()

            If lbl_id.Text = "ID" Then
                Using cmd = New OleDbCommand()
                    cmd.Connection = conn
                    cmd.CommandText = "INSERT INTO tb_config (usuario, senha, tipo, bloqueado) VALUES " &
                    "(@usuario, @senha, @tipo, @bloqueado)"
                    cmd.Parameters.AddWithValue("@usuario", txt_nomeusuario.Text)
                    cmd.Parameters.AddWithValue("@senha", txt_senha.Text)
                    cmd.Parameters.AddWithValue("@tipo", cmbTipoUsuario.SelectedItem)
                    cmd.Parameters.AddWithValue("@bloqueado", status_acesso)
                    cmd.ExecuteNonQuery()
                End Using
                MessageBox.Show("Usuario cadastrado com sucesso!")
            Else
                Using cmd = New OleDbCommand()
                    cmd.Connection = conn
                    cmd.CommandText = "UPDATE tb_config set usuario=@usuario, senha=@senha, tipo=@tipo, bloqueado=@bloqueado WHERE id=@id"
                    cmd.Parameters.AddWithValue("@usuario", txt_nomeusuario.Text)
                    cmd.Parameters.AddWithValue("@senha", txt_senha.Text)
                    cmd.Parameters.AddWithValue("@tipo", cmbTipoUsuario.SelectedItem)
                    cmd.Parameters.AddWithValue("@bloqueado", status_acesso)
                    cmd.Parameters.AddWithValue("@id", Convert.ToInt32(lbl_id.Text))
                    cmd.ExecuteNonQuery()
                End Using
                MessageBox.Show("Usuario atualizado com sucesso!")
            End If

            lbl_id.Text = "ID"
            txt_nomeusuario.Text = ""
            txt_senha.Text = ""
            cmbTipoUsuario.SelectedIndex = 0
            cmb_bloquear.SelectedIndex = 0
            txt_pesquisar.Clear()
            btn_pesquisar_Click(Nothing, Nothing)
        End Using
    End Sub
    Private Sub btn_pesquisar_Click(sender As Object, e As EventArgs) Handles btn_pesquisar.Click
        If Trim(txt_pesquisar.Text) = "" Then
            Dim data As OleDbDataReader = Nothing
            Const sql = "select * from tb_config "
            Using con As OleDbConnection = GetConnection()
                Using cmd As New OleDbCommand(sql, con)
                    con.Open()
                    data = cmd.ExecuteReader()
                    dt = New DataTable
                    dt.Load(data)
                    data.Close()

                    dgw_usuarios.DataSource = dt
                    dgw_usuarios.Refresh()
                End Using
            End Using
        Else
            Dim data As OleDbDataReader = Nothing
            Const sql = "select * from tb_config WHERE usuario = @u"
            Using con As OleDbConnection = GetConnection()
                Using cmd As New OleDbCommand(sql, con)
                    con.Open()
                    cmd.Parameters.AddWithValue("@u", Trim(txt_pesquisar.Text))
                    data = cmd.ExecuteReader()
                    dt = New DataTable
                    dt.Load(data)
                    data.Close()

                    dgw_usuarios.DataSource = dt
                    dgw_usuarios.Refresh()
                End Using
            End Using
        End If

    End Sub

    Private Sub cmb_bloquear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_bloquear.SelectedIndexChanged
        itensComboAcesso = cmb_bloquear.SelectedItem.ToString()
        If itensComboAcesso = "Usuário Liberado" Then
            status_acesso = True
        Else
            status_acesso = False
        End If
    End Sub

    Private Sub txt_pesquisar_Click(sender As Object, e As EventArgs) Handles txt_pesquisar.Click
        txt_pesquisar.Clear()
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        If dgw_usuarios.SelectedCells.Count = 0 Then
            MessageBox.Show("Não existem registros selecionados.")
        Else
            Try
                deleteSelectedItems()
                ReSearch()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    Private Sub deleteSelectedItems()
        Dim sqlDelete As String = "delete from tb_config where id=?"
        Try
            Using conn = GetConnection()
                Using command = New OleDbCommand(sqlDelete, conn)
                    command.Parameters.Add("@p1", OleDbType.Integer)
                    conn.Open()
                    For Each cell As DataGridViewCell In dgw_usuarios.SelectedCells
                        Dim row As DataGridViewRow = dgw_usuarios.Rows(cell.RowIndex)
                        command.Parameters(0).Value = Convert.ToInt32(row.Cells(0).Value)
                        Dim rowAffected As Integer = command.ExecuteNonQuery()
                        dgw_usuarios.Rows.Remove(row)
                    Next
                    conn.Close()
                End Using
            End Using
        Catch ex As System.InvalidOperationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ReSearch()
        txt_pesquisar.Text = ""
        btn_pesquisar_Click(Nothing, Nothing)
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If dgw_usuarios.SelectedCells.Count = 0 Then
            MessageBox.Show("Não existem registros selecionados.")
        Else
            Dim selectedGridRow As DataGridViewRow = dgw_usuarios.Rows(dgw_usuarios.SelectedCells.Item(0).RowIndex)
            Do
                For Each r As DataRow In dt.Rows
                    If r(0) = selectedGridRow.Cells(0).Value Then
                        lbl_id.Text = r("id")
                        txt_nomeusuario.Text = r("usuario")
                        txt_senha.Text = r("senha")
                        cmbTipoUsuario.SelectedItem = r("tipo")
                        If r("bloqueado") Then
                            cmb_bloquear.SelectedIndex = 1
                        Else
                            cmb_bloquear.SelectedIndex = 0
                        End If
                        cmb_bloquear.SelectedItem = r("bloqueado")
                        Exit Do
                    End If
                Next
            Loop While False
        End If
    End Sub

    Private Sub btn_home_Click_1(sender As Object, e As EventArgs) Handles btnHome.Click
        frm_home.Show()
        Me.Close()
    End Sub

    Private Sub btn_sair_Click_1(sender As Object, e As EventArgs) Handles btnSair.Click
        frm_login.Show()
        Me.Close()
    End Sub
End Class