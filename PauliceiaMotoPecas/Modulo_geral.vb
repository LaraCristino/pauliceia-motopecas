﻿Imports System.Data.OleDb
Module Modulo_geral
    Public db As New ADODB.Connection
    Public rs As New ADODB.Recordset
    Public sql, op, aux, resp As String
    Public id, contar As Long
    Public conexao = "|DataDirectory|\database.mdb"
    Public tipoUsuarioLogado As String

    Public Function GetConnection() As OleDbConnection
        Dim conexao As String = "Provider=Microsoft.ACE.OLEDB.12.0;DATA SOURCE=|DataDirectory|\database.mdb"
        Return New OleDbConnection(conexao)
    End Function

    Sub conexao_banco()
        Try
            db = CreateObject("adodb.connection")
            db.Open("Provider=Microsoft.Jet.oledb.4.0;data source= " & conexao)
        Catch ex As Exception
            MsgBox("Alguma coisa deu errado na conexão! Tente novamente mais tarde.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try
    End Sub

    Sub gerar_id_estoque()
        Try
            sql = "select * from tb_produtos order by id_produto desc"
            rs = db.Execute(sql)
            If rs.BOF = True Then
                id = 1
            Else
                id = rs.Fields(0).Value + 1
            End If
        Catch ex As Exception
            MsgBox("Erro no processamento!")
        End Try
    End Sub

    Sub gerar_id_clientes()
        Try
            sql = "select * from tb_clientes order by id_cliente desc"
            rs = db.Execute(sql)
            If rs.BOF = True Then
                id = 1
            Else
                id = rs.Fields(0).Value + 1
            End If
        Catch ex As Exception
            MsgBox("Erro no processamento!")
        End Try
    End Sub

    Sub gerar_id_fornecedores()
        Try
            sql = "select * from tb_fornecedores order by id_fornecedor desc"
            rs = db.Execute(sql)
            If rs.BOF = True Then
                id = 1
            Else
                id = rs.Fields(0).Value + 1
            End If
        Catch ex As Exception
            MsgBox("Erro no processamento!")
        End Try
    End Sub

    Sub dados_grid_estoque()
        With frm_estoque.dgw_estoque
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_produtos order by id_produto asc"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Sub dados_grid_clientes()
        With _05_frm_clientes.dgw_clientes
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_clientes order by id_cliente asc"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, rs.Fields(4).Value, Nothing, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Sub dados_grid_fornecedores()
        With _05_frm_clientes.dgw_fornecedores
            contar = 1
            .Rows.Clear()
            sql = "select * from tb_fornecedores order by id_fornecedor asc"
            rs = db.Execute(sql)
            Do While rs.EOF = False
                .Rows.Add(contar, rs.Fields(1).Value, rs.Fields(2).Value, rs.Fields(3).Value, rs.Fields(4).Value, Nothing, Nothing)
                contar = contar + 1
                rs.MoveNext()
            Loop
        End With
    End Sub

    Sub limpar_dados_config(form As _07_frm_configuracoes)
        With form
            .txt_nomeusuario.Clear()
            .txt_senha.Clear()
            .cmb_bloquear.SelectedIndex = 0
            .txt_nomeusuario.Focus()
        End With
    End Sub
End Module
