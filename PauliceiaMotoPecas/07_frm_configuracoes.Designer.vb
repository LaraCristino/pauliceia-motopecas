﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class _07_frm_configuracoes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(_07_frm_configuracoes))
        Me.txt_senha = New System.Windows.Forms.MaskedTextBox()
        Me.dgw_usuarios = New System.Windows.Forms.DataGridView()
        Me.btn_pesquisar = New System.Windows.Forms.Button()
        Me.txt_pesquisar = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbl_cons = New System.Windows.Forms.Label()
        Me.cmb_bloquear = New System.Windows.Forms.ComboBox()
        Me.txt_nomeusuario = New System.Windows.Forms.TextBox()
        Me.cmbTipoUsuario = New System.Windows.Forms.ComboBox()
        Me.btnExcluirSelecionados = New System.Windows.Forms.Button()
        Me.btnEditarSelecionado = New System.Windows.Forms.Button()
        Me.lbl_id = New System.Windows.Forms.Label()
        Me.btnAdicionarNovo = New System.Windows.Forms.Button()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        CType(Me.dgw_usuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_senha
        '
        Me.txt_senha.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.txt_senha.Location = New System.Drawing.Point(247, 104)
        Me.txt_senha.Mask = "********"
        Me.txt_senha.Name = "txt_senha"
        Me.txt_senha.Size = New System.Drawing.Size(179, 20)
        Me.txt_senha.TabIndex = 28
        '
        'dgw_usuarios
        '
        Me.dgw_usuarios.AllowUserToOrderColumns = True
        Me.dgw_usuarios.BackgroundColor = System.Drawing.Color.Silver
        Me.dgw_usuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw_usuarios.Location = New System.Drawing.Point(45, 194)
        Me.dgw_usuarios.Name = "dgw_usuarios"
        Me.dgw_usuarios.Size = New System.Drawing.Size(542, 218)
        Me.dgw_usuarios.TabIndex = 36
        '
        'btn_pesquisar
        '
        Me.btn_pesquisar.BackColor = System.Drawing.Color.Transparent
        Me.btn_pesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_pesquisar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_pesquisar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btn_pesquisar.Location = New System.Drawing.Point(247, 161)
        Me.btn_pesquisar.Name = "btn_pesquisar"
        Me.btn_pesquisar.Size = New System.Drawing.Size(85, 27)
        Me.btn_pesquisar.TabIndex = 35
        Me.btn_pesquisar.Text = "Pesquisar"
        Me.btn_pesquisar.UseVisualStyleBackColor = False
        '
        'txt_pesquisar
        '
        Me.txt_pesquisar.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.txt_pesquisar.Location = New System.Drawing.Point(45, 165)
        Me.txt_pesquisar.Name = "txt_pesquisar"
        Me.txt_pesquisar.Size = New System.Drawing.Size(196, 20)
        Me.txt_pesquisar.TabIndex = 34
        Me.txt_pesquisar.Text = "Buscar nome"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(42, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 16)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Pequisar usuário:"
        '
        'lbl_cons
        '
        Me.lbl_cons.AutoSize = True
        Me.lbl_cons.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cons.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cons.ForeColor = System.Drawing.Color.White
        Me.lbl_cons.Location = New System.Drawing.Point(42, 76)
        Me.lbl_cons.Name = "lbl_cons"
        Me.lbl_cons.Size = New System.Drawing.Size(199, 16)
        Me.lbl_cons.TabIndex = 32
        Me.lbl_cons.Text = "Cadastro de novo usuário:"
        '
        'cmb_bloquear
        '
        Me.cmb_bloquear.FormattingEnabled = True
        Me.cmb_bloquear.Location = New System.Drawing.Point(432, 103)
        Me.cmb_bloquear.Name = "cmb_bloquear"
        Me.cmb_bloquear.Size = New System.Drawing.Size(156, 21)
        Me.cmb_bloquear.TabIndex = 29
        '
        'txt_nomeusuario
        '
        Me.txt_nomeusuario.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.txt_nomeusuario.Location = New System.Drawing.Point(62, 104)
        Me.txt_nomeusuario.Name = "txt_nomeusuario"
        Me.txt_nomeusuario.Size = New System.Drawing.Size(179, 20)
        Me.txt_nomeusuario.TabIndex = 27
        Me.txt_nomeusuario.Text = "Nome de Usuário"
        '
        'cmbTipoUsuario
        '
        Me.cmbTipoUsuario.FormattingEnabled = True
        Me.cmbTipoUsuario.Location = New System.Drawing.Point(594, 103)
        Me.cmbTipoUsuario.Name = "cmbTipoUsuario"
        Me.cmbTipoUsuario.Size = New System.Drawing.Size(166, 21)
        Me.cmbTipoUsuario.TabIndex = 40
        '
        'btnExcluirSelecionados
        '
        Me.btnExcluirSelecionados.Location = New System.Drawing.Point(45, 418)
        Me.btnExcluirSelecionados.Name = "btnExcluirSelecionados"
        Me.btnExcluirSelecionados.Size = New System.Drawing.Size(209, 23)
        Me.btnExcluirSelecionados.TabIndex = 41
        Me.btnExcluirSelecionados.Text = "Excluir selecionados"
        Me.btnExcluirSelecionados.UseVisualStyleBackColor = True
        '
        'btnEditarSelecionado
        '
        Me.btnEditarSelecionado.Location = New System.Drawing.Point(374, 418)
        Me.btnEditarSelecionado.Name = "btnEditarSelecionado"
        Me.btnEditarSelecionado.Size = New System.Drawing.Size(213, 23)
        Me.btnEditarSelecionado.TabIndex = 42
        Me.btnEditarSelecionado.Text = "Editar selecionado"
        Me.btnEditarSelecionado.UseVisualStyleBackColor = True
        '
        'lbl_id
        '
        Me.lbl_id.AutoSize = True
        Me.lbl_id.BackColor = System.Drawing.Color.Transparent
        Me.lbl_id.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_id.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_id.Location = New System.Drawing.Point(42, 107)
        Me.lbl_id.Name = "lbl_id"
        Me.lbl_id.Size = New System.Drawing.Size(22, 13)
        Me.lbl_id.TabIndex = 37
        Me.lbl_id.Text = "ID"
        '
        'btnAdicionarNovo
        '
        Me.btnAdicionarNovo.Location = New System.Drawing.Point(608, 242)
        Me.btnAdicionarNovo.Name = "btnAdicionarNovo"
        Me.btnAdicionarNovo.Size = New System.Drawing.Size(132, 23)
        Me.btnAdicionarNovo.TabIndex = 43
        Me.btnAdicionarNovo.Text = "Adicionar / Novo"
        Me.btnAdicionarNovo.UseVisualStyleBackColor = True
        '
        'btnHome
        '
        Me.btnHome.Location = New System.Drawing.Point(608, 161)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(75, 23)
        Me.btnHome.TabIndex = 44
        Me.btnHome.Text = "Home"
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(608, 194)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 45
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        '_07_frm_configuracoes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(800, 473)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnHome)
        Me.Controls.Add(Me.btnAdicionarNovo)
        Me.Controls.Add(Me.btnEditarSelecionado)
        Me.Controls.Add(Me.btnExcluirSelecionados)
        Me.Controls.Add(Me.cmbTipoUsuario)
        Me.Controls.Add(Me.lbl_id)
        Me.Controls.Add(Me.txt_senha)
        Me.Controls.Add(Me.dgw_usuarios)
        Me.Controls.Add(Me.btn_pesquisar)
        Me.Controls.Add(Me.txt_pesquisar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbl_cons)
        Me.Controls.Add(Me.cmb_bloquear)
        Me.Controls.Add(Me.txt_nomeusuario)
        Me.Name = "_07_frm_configuracoes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "_07_frm_configuracoes"
        CType(Me.dgw_usuarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_senha As MaskedTextBox
    Friend WithEvents dgw_usuarios As DataGridView
    Friend WithEvents btn_pesquisar As Button
    Friend WithEvents txt_pesquisar As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents lbl_cons As Label
    Friend WithEvents cmb_bloquear As ComboBox
    Friend WithEvents txt_nomeusuario As TextBox
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents DataSet1 As DataSet1
    Friend WithEvents Tb_configTableAdapter As DataSet1TableAdapters.tb_configTableAdapter
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UsuarioDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SenhaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BloqueadoDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents cmbTipoUsuario As ComboBox
    Friend WithEvents btnExcluirSelecionados As Button
    Friend WithEvents btnEditarSelecionado As Button
    Friend WithEvents lbl_id As Label
    Friend WithEvents btnAdicionarNovo As Button
    Friend WithEvents btnHome As Button
    Friend WithEvents btnSair As Button
End Class
