﻿Public Class frm_home
    Private Sub frm_home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frm_login.Hide()

        If tipoUsuarioLogado = "LIMITADO" Then
            lbl_tipousuario.Text = "LIMITADO"
        ElseIf tipoUsuarioLogado = "INTERMEDIARIO" Then
            lbl_tipousuario.Text = "INTERMEDIARIO"
            btn_configuracoes.Enabled = False
        Else
            lbl_tipousuario.Text = "ADMINISTRADOR"
        End If
    End Sub
    Private Sub btn_estoque_Click_1(sender As Object, e As EventArgs) Handles btn_estoque.Click
        If tipoUsuarioLogado = "LIMITADO" Then
            MsgBox("Disponível apenas ara usuário ADMINISTRADOR")
        Else
            frm_estoque.Show()
            Me.Close()
        End If
    End Sub

    Private Sub btn_vendas_Click_1(sender As Object, e As EventArgs) Handles btn_vendas.Click
        _02_frm_vendas.Show()
        Me.Close()
    End Sub
    Private Sub btn_clientes_Click(sender As Object, e As EventArgs) Handles btn_clientes.Click
        _05_frm_clientes.Show()
        Me.Close()
    End Sub

    Private Sub btn_suporte_Click(sender As Object, e As EventArgs) Handles btn_suporte.Click
        _08_frm_suportes.Show()
        Me.Close()
    End Sub

    Private Sub btn_configuracoes_Click(sender As Object, e As EventArgs) Handles btn_configuracoes.Click
        If tipoUsuarioLogado = "INTERMEDIARIO" Or tipoUsuarioLogado = "LIMITADO" Then
            MsgBox("Disponível apenas ara usuário ADMINISTRADOR")
        Else
            _07_frm_configuracoes.Show()
            Me.Close()
        End If
    End Sub

    Private Sub btn_sair_Click_1(sender As Object, e As EventArgs) Handles btn_sair.Click
        frm_login.Show()
        Me.Close()
    End Sub

    Private Sub Label9_Click(sender As Object, e As EventArgs) Handles Label9.Click

    End Sub
End Class